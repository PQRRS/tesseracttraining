#!/bin/bash

# Prepare output dir.
mkdir Generated

# CMC7 font.
tesseract ./Boxing/cst/CMC7_Real1.tif         ./Generated/cst.cmc7.exp0 nobatch box.train.stderr
tesseract ./Boxing/cst/CMC7_Real2.tif         ./Generated/cst.cmc7.exp1 nobatch box.train.stderr
# # OCRB font.
tesseract ./Boxing/cst/OCRB_Real1.png         ./Generated/cst.ocrb.exp0 nobatch box.train.stderr
tesseract ./Boxing/cst/OCRB_Full1.png         ./Generated/cst.ocrb.exp1 nobatch box.train.stderr
# Other standard fonts.
tesseract ./Boxing/fra/fra.arial.tif          ./Generated/cst.arial.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.arialbd.tif       ./Generated/cst.arialbd.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.arialbi.tif       ./Generated/cst.arialbi.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.ariali.tif        ./Generated/cst.ariali.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.b018012l.tif      ./Generated/cst.b018012l.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.b018015l.tif      ./Generated/cst.b018015l.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.b018032l.tif      ./Generated/cst.b018032l.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.b018035l.tif      ./Generated/cst.b018035l.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.c059013l.tif      ./Generated/cst.c059013l.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.c059016l.tif      ./Generated/cst.c059016l.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.c059033l.tif      ./Generated/cst.c059033l.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.c059036l.tif      ./Generated/cst.c059036l.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.cour.tif          ./Generated/cst.cour.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.courbd.tif        ./Generated/cst.courbd.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.courbi.tif        ./Generated/cst.courbi.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.couri.tif         ./Generated/cst.couri.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.georgia.tif       ./Generated/cst.georgia.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.georgiab.tif      ./Generated/cst.georgiab.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.georgiai.tif      ./Generated/cst.georgiai.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.georgiaz.tif      ./Generated/cst.georgiaz.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.times.tif         ./Generated/cst.times.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.timesbd.tif       ./Generated/cst.timesbd.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.timesbi.tif       ./Generated/cst.timesbi.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.timesi.tif        ./Generated/cst.timesi.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.trebuc.tif        ./Generated/cst.trebuc.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.trebucbd.tif      ./Generated/cst.trebucbd.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.trebucbi.tif      ./Generated/cst.trebucbi.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.trebucit.tif      ./Generated/cst.trebucit.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.verdana.tif       ./Generated/cst.verdana.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.verdanab.tif      ./Generated/cst.verdanab.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.verdanai.tif      ./Generated/cst.verdanai.exp0 nobatch box.train.stderr
tesseract ./Boxing/fra/fra.verdanaz.tif      ./Generated/cst.verdanaz.exp0 nobatch box.train.stderr

# Cleanup.
rm Generated/cst.*.*.txt
