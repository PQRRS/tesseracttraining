#!/bin/bash

cd Generated

unicharset_extractor  ../Boxing/CMC7_Real1.box \
                      ../Boxing/CMC7_Real2.box \
                      ../Boxing/OCRB_Real1.box \
                      ../Boxing/OCRB_Full1.box \
                      ../Boxing/fra/fra.arial.box \
                      ../Boxing/fra/fra.arialbd.box \
                      ../Boxing/fra/fra.arialbi.box \
                      ../Boxing/fra/fra.ariali.box \
                      ../Boxing/fra/fra.b018012l.box \
                      ../Boxing/fra/fra.b018015l.box \
                      ../Boxing/fra/fra.b018032l.box \
                      ../Boxing/fra/fra.b018035l.box \
                      ../Boxing/fra/fra.c059013l.box \
                      ../Boxing/fra/fra.c059016l.box \
                      ../Boxing/fra/fra.c059033l.box \
                      ../Boxing/fra/fra.c059036l.box \
                      ../Boxing/fra/fra.cour.box \
                      ../Boxing/fra/fra.courbd.box \
                      ../Boxing/fra/fra.courbi.box \
                      ../Boxing/fra/fra.couri.box \
                      ../Boxing/fra/fra.georgia.box \
                      ../Boxing/fra/fra.georgiab.box \
                      ../Boxing/fra/fra.georgiai.box \
                      ../Boxing/fra/fra.georgiaz.box \
                      ../Boxing/fra/fra.times.box \
                      ../Boxing/fra/fra.timesbd.box \
                      ../Boxing/fra/fra.timesbi.box \
                      ../Boxing/fra/fra.timesi.box \
                      ../Boxing/fra/fra.trebuc.box \
                      ../Boxing/fra/fra.trebucbd.box \
                      ../Boxing/fra/fra.trebucbi.box \
                      ../Boxing/fra/fra.trebucit.box \
                      ../Boxing/fra/fra.verdana.box \
                      ../Boxing/fra/fra.verdanab.box \
                      ../Boxing/fra/fra.verdanai.box \
                      ../Boxing/fra/fra.verdanaz.box
