#!/bin/bash

cd Generated

mftraining  -F ../Fonts/font_properties \
            -U unicharset \
            -O cst.unicharset \
            cst.cmc7.exp0.tr \
            cst.cmc7.exp1.tr \
            cst.ocrb.exp0.tr \
            cst.ocrb.exp1.tr \
            cst.arial.exp0.tr \
            cst.arialbd.exp0.tr \
            cst.arialbi.exp0.tr \
            cst.ariali.exp0.tr \
            cst.b018012l.exp0.tr \
            cst.b018015l.exp0.tr \
            cst.b018032l.exp0.tr \
            cst.b018035l.exp0.tr \
            cst.c059013l.exp0.tr \
            cst.c059016l.exp0.tr \
            cst.c059033l.exp0.tr \
            cst.c059036l.exp0.tr \
            cst.cour.exp0.tr \
            cst.courbd.exp0.tr \
            cst.courbi.exp0.tr \
            cst.couri.exp0.tr \
            cst.georgia.exp0.tr \
            cst.georgiab.exp0.tr \
            cst.georgiai.exp0.tr \
            cst.georgiaz.exp0.tr \
            cst.times.exp0.tr \
            cst.timesbd.exp0.tr \
            cst.timesbi.exp0.tr \
            cst.timesi.exp0.tr \
            cst.trebuc.exp0.tr \
            cst.trebucbd.exp0.tr \
            cst.trebucbi.exp0.tr \
            cst.trebucit.exp0.tr \
            cst.verdana.exp0.tr \
            cst.verdanab.exp0.tr \
            cst.verdanai.exp0.tr \
            cst.verdanaz.exp0.tr
