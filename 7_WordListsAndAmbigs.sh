#!/bin/bash

cd Wordlist

wordlist2dawg   Wordlist.Punctuation.txt  ../Generated/cst.punc-dawg    ../Generated/unicharset
wordlist2dawg   Wordlist.Word.txt         ../Generated/cst.word-dawg    ../Generated/unicharset
wordlist2dawg   Wordlist.Number.txt       ../Generated/cst.number-dawg  ../Generated/unicharset
wordlist2dawg   Wordlist.Frequent.txt     ../Generated/cst.freq-dawg    ../Generated/unicharset

cp              Disambiguation.txt        ../Generated/cst.unicharambigs
