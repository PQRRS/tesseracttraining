#!/bin/bash

cd Generated

cp inttemp    cst.inttemp
cp normproto  cst.normproto
cp pffmtable  cst.pffmtable
cp shapetable cst.shapetable

combine_tessdata cst.

rm  cst.inttemp \
    cst.normproto \
    cst.pffmtable \
    cst.shapetable

mv cst.traineddata ../tessdata/
